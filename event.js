/////////////////////////////////////////////////////////////////////////////////////////////
//
// event
//
//    Library for emitting events.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var singleton;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Event Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function Event() {
    var self = this;

    this.Emitter = function (context) {
        var own = this;
        var callbacks = [];

        this.on = function (type, callback) {
            if (Object.prototype.toString.call(type) === "[object Array]") {
                for (var t in type) {
                    own.on(type[t], callback);
                }
                return;
            }
            var clb = {"callback": callback, "type": type};
            clb.id = callbacks.push(clb) - 1;

            return clb.id;
        };
        this.addEventListener = this.on;
        if (context && !context.addEventListener) {
            context.addEventListener = this.on;
        }
        if (context && !context.on) {
            context.on = this.on;
        }

        this.removeEventListener = function (id) {
            if (Object.prototype.toString.call(id) === "[object Function]") {
                for (var i in callbacks) {
                    if (callbacks[i].callback == id) {
                        callbacks.splice(i, 1);
                        i--;
                    }
                }
            }
            else {
                callbacks.splice(id, 1);
            }
        };
        if (context && !context.removeEventListener) {
            context.removeEventListener = this.removeEventListener;
        }

        this.fire = function (type, opt_arg) {
            for (var e = 0; e < callbacks.length; e++) {
                if (callbacks[e] != null && (callbacks[e].type == type || callbacks[e].type == "*")) {
                    try {
                        var a = [];
                        for (var i in arguments) {
                            if (i == 0) {
                                continue;
                            }
                            a.push(arguments[i]);
                        }
                        if (callbacks[e].type == "*") {
                            a.unshift(type);
                            a.unshift(context || global || window);
                        }
                        var retVal = callbacks[e].callback.apply(context, a);
                        if (retVal == true) {
                            return true;
                        }
                    }
                    catch (ex) {
                        if (Object.prototype.toString.call(callbacks[e].callback) !== "[object Function]") {
                            context.removeEventListener(e);
                            e--;
                        }
                    }
                }
            }
        };
    };
    this.Progress = function (progress) {
        this.percentage = progress ? progress.percentage : 0;
        this.operation = new self.Operation(progress ? progress.operation : null);
        this.emitter = progress ? progress.emitter : null;
    };
    this.Operation = function (operation) {
        this.type = operation ? operation.type : null;
        this.data = operation ? operation.data : null;
    };
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Singleton Instance
//
/////////////////////////////////////////////////////////////////////////////////////////////
singleton = new Event();

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = singleton;